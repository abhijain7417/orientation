# Git Summary

## Concept of GIT

GIT is the distributed version control system which means the main copy of the code will be stored in a remote repository (GIT server). And, every time when the developer wants to work on the code, the developer will clone the code to the local repository (GIT client). Then the developer will create a branch from the main codebase and make changes. Once changes are done, Developer will update the code in a remote repository.

![](https://i1.wp.com/digitalvarys.com/wp-content/uploads/2019/06/image-4.png?w=1156&ssl=1)

### GIT Local

We can say the GIT Local or GIT client as three major parts called

* The Development Working directory
* The Staging Area
* The Local Repository

### GIT Remote 

Remote Repository is stored in GIT remote server or centralized server, where the main copy of the code is been stored. After the commit to the local repository, Developer will send the changed code to the remote server by passing push command of GIT Client tool.

## GIT Workflow

As part of the GIT Basics and Beginners Guide, we will discuss the important process flow of GIT called GIT workflow. GIT workflow is the GIT process cycle of versioning and version control. Where it starts from getting the code from the remote (master) repository to pushing back the changed code from local to the remote repository. Let’s see all in detail.

### GIT clone, GIT pull and GIT Fetch

GIT clone is the process of creating a working copy of the remote or local repository by passing the following command.

                 git clone <path_of_repository>

If we have already cloned the repository and need to update local (only code) respect to the remote server, we need to get pull from the remote server by passing following command

                 git pull origin master

When in the above command, git pull is the command, the origin is the remote reference/URL of remote server and master is the branch name.

GIT fetch is the process of updating (only git information) the local GIT structure and information from remote repository. By passing the following command, we can fetch the remote repository.

                 git fetch <remote>

### GIT Checkout

Git checkout is the event of getting or changing the current state of the git branch to another. When we want to create a branch and move to the created branch, we use the following command.

                 git checkout -b <new_branch>


this will create a branch called <new_branch> and current HEAD will move to the newly created branch. Which means, the changes after this command will be captured in the newly created branch.

### GIT Add and GIT Commit

When we want to add the changes of code to the index of the GIT, we will pass the following command.

                 git add <file1> <file2> <file3>

GIT commit is the event of adding the index to the HEAD of the local repository. This will be done by passing following command

                 git commit -m “your commit message”

But, this commit will be done only on the local repository. Which mean, we need to push the commits and updated HEAD to the remote repository.

![](https://i1.wp.com/digitalvarys.com/wp-content/uploads/2019/06/image-3.png?w=1129&ssl=1)

### GIT push

When we commit changes to the local repository, it will have the information about changes in the codebase and its change message with it. So, if we want to update or send the changes to the remote repository, we need to pass the following command.

                 git push origin <new_branch>

Like we have seen in GIT Pull, the origin is the alias name of the location of the remote server. When we point the <new_branch>, GIT push command will create a new branch in the remote server and store the changes.

### GIT Merge

GIT Merge will merge the update in <new_branch> to the master branch or whatever the branch we need to merge with. To merge the branch with the current branch. Checkout to the working branch and pass the following command

                 git merge <master_or_destination_branch>

This will merge and add the changes that are present in the <new_branch> to <master_or_destination_branch>. Ideally, this process will be taken care of by hosting service tools like Gitlab or BitBucket.

This is the basic workflow, but this is not the end of the workflow. We need to merge the <new_branch> to the master branch.

## References

[Official reference link](https://git-scm.com/docs/git)
