# Docker Summary

Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. 

## Docker Engine

Docker Engine is a client-server application with these major components:

* A server which is a type of long-running program called a daemon process (the dockerd command).

* A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do.

* A command line interface (CLI) client (the docker command).

![](https://docs.docker.com/engine/images/engine-components-flow.png)

## Docker Architecture

![](https://docs.docker.com/engine/images/architecture.svg)

### The Docker daemon

The Docker daemon (dockerd) listens for Docker API requests and manages Docker objects such as images, containers, networks, and volumes. A daemon can also communicate with other daemons to manage Docker services.

### The Docker client

The Docker client (docker) is the primary way that many Docker users interact with Docker. When you use commands such as docker run, the client sends these commands to dockerd, which carries them out. The docker command uses the Docker API. The Docker client can communicate with more than one daemon.

### Docker registries

A Docker registry stores Docker images. Docker Hub is a public registry that anyone can use, and Docker is configured to look for images on Docker Hub by default. You can even run your own private registry.

When you use the docker pull or docker run commands, the required images are pulled from your configured registry. When you use the docker push command, your image is pushed to your configured registry.

### Docker objects

When you use Docker, you are creating and using images, containers, networks, volumes, plugins, and other objects. This section is a brief overview of some of those objects.

#### IMAGES

An image is a read-only template with instructions for creating a Docker container. Often, an image is based on another image, with some additional customization. For example, you may build an image which is based on the ubuntu image, but installs the Apache web server and your application, as well as the configuration details needed to make your application run.

You might create your own images or you might only use those created by others and published in a registry. To build your own image, you create a Dockerfile with a simple syntax for defining the steps needed to create the image and run it. Each instruction in a Dockerfile creates a layer in the image. When you change the Dockerfile and rebuild the image, only those layers which have changed are rebuilt. This is part of what makes images so lightweight, small, and fast, when compared to other virtualization technologies.

#### CONTAINERS

A container is a runnable instance of an image. You can create, start, stop, move, or delete a container using the Docker API or CLI. You can connect a container to one or more networks, attach storage to it, or even create a new image based on its current state.

By default, a container is relatively well isolated from other containers and its host machine. You can control how isolated a container’s network, storage, or other underlying subsystems are from other containers or from the host machine.

## Install Docker Engine on Ubuntu

### Uninstall old versions

                 $ sudo apt-get remove docker docker-engine docker.io containerd runc

### Install using the repository

1. Update the apt package index and install packages to allow apt to use a repository over HTTPS:

                 $ sudo apt-get update

                 $ sudo apt-get install \
                   apt-transport-https \
                   ca-certificates \
                   curl \
                   gnupg-agent \
                   software-properties-common

2. Add Docker’s official GPG key:       

                 $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

Verify that you now have the key with the fingerprint 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88, by searching for the last 8 characters of the fingerprint.

                 $ sudo apt-key fingerprint 0EBFCD88

                 pub   rsa4096 2017-02-22 [SCEA]
                 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
                 uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
                 sub   rsa4096 2017-02-22 [S]

3. Use the following command to set up the stable repository. To add the nightly or test repository, add the word nightly or test (or both) after the word stable in the commands below.

                  $ sudo add-apt-repository \
                  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
                  $(lsb_release -cs) \
                   stable"

### INSTALL DOCKER ENGINE

1. Update the apt package index, and install the latest version of Docker Engine and containerd, or go to the next step to install a specific version:          

                  $ sudo apt-get update
                  $ sudo apt-get install docker-ce docker-ce-cli containerd.io


2. To install a specific version of Docker Engine, list the available versions in the repo, then select and install:

   * List the versions available in your repo:

                  $ apt-cache madison docker-ce

                  docker-ce | 5:18.09.1~3-0~ubuntu-xenial | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
                  docker-ce | 5:18.09.0~3-0~ubuntu-xenial | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
                  docker-ce | 18.06.1~ce~3-0~ubuntu       | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
                  docker-ce | 18.06.0~ce~3-0~ubuntu       | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
                  ...

   *    Install a specific version using the version string from the second column, for example, 5:18.09.1~3-0~ubuntu-xenial.

                  $ sudo apt-get install docker-ce=<VERSION_STRING> docker-ce-cli=<VERSION_STRING> containerd.io

3. Verify that Docker Engine is installed correctly by running the hello-world image.    

                  $ sudo docker run hello-world

This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits.

## References

[Docker overview](https://docs.docker.com/get-started/overview/)

[Docker install on ubantu](https://docs.docker.com/engine/install/ubuntu/)
                  